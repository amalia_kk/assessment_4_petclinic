# Petclinic Containerised

In this repo you will find:
  - Petclinic application
  - Dockerfile that creates a Petclinic image
  - Docker-Compose that creates container fro Petclinic app and Postgres DB
  - Kubernetes yaml files

## Docker

To create the Petclinic and postgres containers locally run:

`docker-compose up -d`

To see the status of the petclinic container run:

`docker logs petclinic_petclinic_1`

It will take a few minutes to configure Petclinic application. Once it's one, you'll be able to see Petclinic on localhost:8080

Currently both the db and the app have persistent volumes (backed up data)


#### Docker Issue

Currently we are unable to connect the app and db. Some of the issues were caused by the M1 chips in our laptops. We were unable to run petclinic with postgres db selcted or configured manually. We also tried using mysql and ran into the same issues.


## Kubernetes

Login into the K8 control panel and run the following command to get started:

`sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf`

In the folder petclinc, you will find all the yaml files in this repo used to configure the pods.


You will be able to access Petclinic here: 
http://petclinic-kaf.academy.labs.automationlogic.com/

After deployment it could take a few minutes to load up.

## Piepline

When developers are working on the project ensure they use a branch that starts dev. This is to ensure tests are done on the code before it's deployed live.